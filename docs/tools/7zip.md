WindowsのPCを使う上で必須級のソフトウェアです。

よく見る.zipや.7zのファイル、いわゆる圧縮ファイルと言われるものを展開するのに使われます。

Windowsにも標準で備わっていますが、速度が段違いなのでこちらをおすすめします。

### １．インストール方法

![7zip HP Screen shot](../assets/7z1.jpg)

まず、[7-zip.org](https://7-zip.org/)にアクセスして、画像内の赤矢印で示してある64-bit(x64)バージョンの7-zipをダウンロードしてください。

![7zip File Screen shot](../assets/7z2.png)

次に、ダウンロードしたファイルを開いてください。

ユーザーアカウント制御の黄色い画面が出てくると思うので、でてきたら「はい」を押してください。

![7zip Installer Screen shot](../assets/7z3.jpg)

次に、このようなウィンドウが出てくるでしょう。これもそのままInstallを押していただいて構いません。

![7zip Installer Screen shot](../assets/7z4.jpg)

この様になったらインストール完了です。Closeを押して閉じてください。

### ２-１．使い方(解凍)

![7zip kaito Screen shot](../assets/7z5.jpg)

zipファイルや7zファイル等の圧縮ファイルで右クリックすると、上記のようなメニューが開きますので、その中の7-Zipからここに展開を押してください。

### ２-２．使い方(圧縮)

![7zip assyuku Screen shot](../assets/7z6.jpg)

フォルダで右クリックすると、上記のようなメニューが開きますので、その中の7-Zipから"◯◯◯.zipに圧縮"を押してください。