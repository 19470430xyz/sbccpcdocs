!!! warning "警告"
    このページは7-Zipがインストールされている前提で作成されています。

    7-Zipをまだインストールしていない場合は、[このページ](../tools/7zip.md)を参考にしてみてください。

はじめに、Teamsの共有フォルダの中に入っている"aviutl-customize3.zip"をダウンロードしてください。

![AviUtl kaitou Screen shot](../assets/austup1.jpg)

ダウンロードしたら、"aviutl-customize3.zip"を展開してください。

これだけでインストールは完了です。

展開してできたaviutl-customize3フォルダはデスクトップなどわかりやすい場所に移動しておくと便利です。

### 次 [簡単な操作](htue.md)